package Osberg;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Q1_StudentGradeCollector {
	
	public static SQLConnectionHandler sqlhandler = new SQLConnectionHandler();
	public String[] grade_lookup = new String[100];// Score equivalence Array A[index=Score]=value#A+,A,...
	public HashMap<Integer, String> HonorRoll = new  HashMap<Integer, String>(); 
	public HashMap<Integer, String> StudentGrades = new HashMap<Integer,String>(); 
	public Q1_StudentGradeCollector() {
		//suppose our HonorRoll is initialized with all students ID and value=""
		for(int i =0 ; i<100 ; i++)
		{
			if (i == 97 || i == 98 || i == 99 || i == 100) 
		    {
				grade_lookup[i]="A+";
		    }
		    else if (i == 87 || i == 88 || i == 89)
		    {
		    	grade_lookup[i]= "B+";
		    }
		    else if (i == 77 || i == 78 || i == 79)
		    {
		    	grade_lookup[i]= "C+";
		    }
		    else if (i == 67 || i == 68 || i == 69)
		    {
		    	grade_lookup[i]= "i+";
		    }
		    else if (i == 93 || i == 94 || i == 95 || i == 96)
		    {
		    	grade_lookup[i]= "A";
		    }
		    else if (i == 83 || i == 84 || i == 85 || i == 86)
		    {
		    	grade_lookup[i]= "B";
		    }
		    else if (i == 73 || i == 74 || i == 75 || i == 76)
		    {
		    	grade_lookup[i]= "C";
		    }
		    else if (i == 63 || i == 64 || i == 65 || i == 66)
		    {
		    	grade_lookup[i]= "D";
		    }
		    else if (i == 90 || i == 91 || i == 92)
		    {
		    	grade_lookup[i]= "A-";
		    }
		    else if (i == 80 || i == 81 || i == 82)
		    {
		    	grade_lookup[i]= "B-";
		    }
		    else if (i == 70 || i == 71 || i == 72)
		    {
		    	grade_lookup[i]= "C-";
		    }
		    else if (i == 60 || i == 61 || i == 62)
		    {
		    	grade_lookup[i]= "D-";
		    }
		    else if (i < 60)
		    {
		    	grade_lookup[i]= "F";
		    }
		}
	}
	public  void UpdateScores()
	{
		for (Integer keyInteger : StudentGrades.keySet()) {
			Integer score = sqlhandler.getGrade(keyInteger); 
			StudentGrades.put(keyInteger, grade_lookup[score]);
			if(grade_lookup[score]=="A+")
			{
				HonorRoll.put(keyInteger, "A+");
			}
		}
	}
}
class SQLConnectionHandler // Setting up the SQL connection string, and includes methods to retrieve information from our MySQL database 	 
{
	  private Connection con = null;
	  private String url = "localhost";
	  private String db = "student_db";
	  private String username = "username";
	  private String passsword = "pass";
	  public int getGrade(Integer studentId )//using this method we get the score of student with student Id= sdeutntid < which is passed as an argument> 
	  {
		  Integer score =-1;
		  try
		  {
			  con = DriverManager.getConnection(url+db, username, passsword);
			  java.sql.PreparedStatement s = con.prepareStatement("SELECT SCORE FROM  STUDENT WHERE ST_ID= ?");
			  s.setString(1,studentId.toString());
			  ResultSet res = s.executeQuery();
			  while (res.next()) 
			  {
				  score = res.getInt(0);
			  }
		  }
		  catch (SQLException s)
		  {
			  System.out.println("Error in SQL Statement.");
		  }  
		  return score;
	  }
}
