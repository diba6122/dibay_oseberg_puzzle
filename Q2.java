package Osberg;

import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

import java.lang.String;
public class Q2 {	
	public static void main(String[] argv)
	{
		Url url = null; 
		String[] url_list  = {"https://www.google.com:8080/images/search?newwindow=1&rlz=1C1CHFX_enUS512US512","https://code.google.com/p/t2framework/wiki/JUnitQuickTutorial","https://www.google.com/search?newwindow=1&rlz=1C1CHFX_enUS512US512&q=how+is+the+weather+today&oq=how+is+the+weather+today&gs_l=serp.3..0l10.7038611.7041976.0.7042197.24.16.0.8.8.0.141.1340.12j4.16.0....0...1c.1.25.serp..0.24.1439.u9Wf5CKeJ-Y"
				,"https://mail.google.com/mail/u/0/?tab=wm","http://stackoverflow.com/questions/17342983/display-syntax-trees-in-webpage/17343186#17343186"};
		for (String address : url_list) {
			System.out.println("Url: "+address);
			url = new Url(address);
			System.out.println("Protocol: "+url.protocol());
			System.out.println("Hostname: "+url.hostname());
			if(url.portnumber()==null)
			{
				System.out.println("Port Number: No port number");
			}
			else
			{
				System.out.println("Port Number: "+url.portnumber().toString());
			}
			if(url.path()==null)
			{
				System.out.println("Path: No path found");	
			}
			else
			{
				System.out.println("Path: "+url.path());	
			}
			if(url.query()==null)
			{
				System.out.println("Query: No query found");	
			}
			else
			{
				System.out.println("Query: "+url.query());	
			}
			System.out.println("\n-----------------------------------\n-----------------------------------\n");
		}
	}
}
