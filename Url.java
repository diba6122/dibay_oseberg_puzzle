/*
 * References
 * http://www.ietf.org/rfc/rfc1738.txt
 * Took: 1h:20min 
 * */


package Osberg;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.URLDecoder;

public class Url
{
	//set of regular expressions for host, protoco, port, path, and query patterns in url @rfc1738 
	final static Pattern Protocol_pattern = Pattern.compile( "\\w+://");
	final static Pattern Hostname_pattern = Pattern.compile(":/+([A-Za-z1-9]|\\.)*[^:\\s/?#]+");
	final static Pattern Portnumber_pattern = Pattern.compile(":\\d+");
	final static Pattern Path_pattern =  Pattern.compile("(/([A-Za-z0-9]|\\.)+)+[^?q=]"); 
	final static Pattern Query_pattern =  Pattern.compile("\\?(([\\w|\\W|\\d|\\D])+=([\\w|\\W|\\d|\\D])+&*)+");
	private Matcher matcher =null;
	private String url = null;
	private String protocol=null; 
	private String hostname = null; 
	private String path  = null ;
	private Integer portnumber  = null;
	private HashMap<String, String> query  =  null;
	
	@SuppressWarnings("deprecation")
	public Url(String url)
	{
		this.url = URLDecoder.decode(url);
	}
	public String protocol()
	{
		matcher = Protocol_pattern.matcher(url);
		if(matcher.find())
		{
			protocol= matcher.group().split(":")[0];
			url = url.replace(protocol, "");
		}
		if(protocol==null)
		{
			return "URL is not compatible with rfc1738 standard.";
		}
		else
		{
			return protocol;
		}
	}
	public String hostname() //extracting the host, from the url , and through an error if the url is not compatible with rfc1738
	{
		matcher = Hostname_pattern.matcher(url);
		if(matcher.find())
		{
			hostname= matcher.group().split("//")[1];
			url= url.replace(hostname, "");
		}
		if(hostname==null)
		{
			System.out.println("URL is not compatible with rfc1738 standard.");
			System.exit(0);
		}
		return hostname;
	}
	public Integer portnumber() //extract the portnumber from url and return it as an Integer value
	{
		matcher = Portnumber_pattern.matcher(url);
		if(matcher.find())
		{
			String temp = matcher.group().toString();
			temp= temp.replace(":", "");
			if((temp.length()<=5)&&(temp.length()>=1))
			{
				portnumber= Integer.parseInt(temp);
				url=url.replace(portnumber.toString(), "");
			}
			else
			{
				System.out.println("URL is not compatible with rfc1738 standard.");
				System.exit(0);
			}
		}
		return portnumber;
	}
	public String path()
	{
		matcher = Path_pattern.matcher(url);
		if(matcher.find())
		{
			String temp = matcher.group().toString();
			if(temp.length()>1)
			{
				path = temp;
				url = url.replace(path, "");
			}
		}
		return path;
	}
	public HashMap<String, String> query()//extract the query and return it as a HashMap<String,String>
	{
		matcher =  Query_pattern.matcher(url);
		if(matcher.find())
		{
			query= new HashMap<String, String>();
			String temp = matcher.group().toString();
			if(temp.length()>1)
			{
				temp = temp.replace("?", "");
				String[] temp2 = temp.split("&");
				for (String pair : temp2) {
					if(pair.contains("="))
					{
						query.put(pair.split("=")[0],pair.split("=")[1]);	
					}	
				}
			}
		}
		return query;
	}
}