package Osberg;

import java.io.File;
import java.util.HashMap;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
public class TestFailure
{
	public static Url url1_tester ;
	
	
	@BeforeClass
	public static void testSetup()
	{
		url1_tester = new Url("https://www.google.com:8080/images/search?newwindow=1&rlz=1C1CHFX_enUS512US512");
	}
	@Test
	public void testurl1_protocol()
	{	
		assertEquals("protocol", "https",url1_tester.protocol());
	}
	@Test
	public void testurl1_hostname()
	{	
		assertEquals("hostname", "www.google.com",url1_tester.hostname());
	}
	@Test
	public void testurl1_portnumber()
	{	
		assertTrue(8080==url1_tester.portnumber());
	}
	@Test
	public void testurl1_path()
	{	
		assertEquals("path", "/images/search",url1_tester.path());
	}
	@Test
	public void testur_query()
	{	
		assertEquals("query", "1",url1_tester.query().get("newwindow"));
		assertEquals("query", "1C1CHFX_enUS512US512",url1_tester.query().get("rlz"));
	}
	public static void main(String[] args)
	{
		TestRunner.runAndWait(new TestSuite(TestFailure.class));
	}
}